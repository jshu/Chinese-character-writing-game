# Chinese Character Writing Game 

## Description
- Design a webpage to help people learn how to write Chinese characters.
- Build a user interface to let user use touch screen as input. 
- Give user feedback to help them improve writing.

## Instruction
This webpage has been published at <http://web.cecs.pdx.edu/~jshu/Chinese-character-writing-game/project.html>.

For local testing, run `node project.js` in the node.js enviroment. Then open the browser and visit "http://localhost:8080/project.html".

## Issue
- Need to use or build a Chinese character bitmap database.
    - Group characters by strokes.
    - Add translation and pronunciation. (May use Google cloud API)
- Better grading algorithm.

## Reference
The signin page referred Firebase Authentication API document. For detail, visit ["Authenticate with Firebase using Password-Based Accounts using Javascript".](https://firebase.google.com/docs/auth/web/password-auth?utm_campaign=Firebase_featureoverview_education_auth_en_07-26-16&utm_source=Firebase&utm_medium=yt-desc)

## License
See [LICENSE](https://gitlab.com/jshu/Chinese-character-writing-game/blob/master/LICENSE)

## Contact
Shu Jiang: jshu@pdx.edu